import com.fasterxml.jackson.databind.ObjectMapper;
import listeners.Message;
import listeners.Ready;
import listeners.commandListeners.Mastermind;
import listeners.commandListeners.Math;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;

import java.io.IOException;
import java.io.InputStream;

/**
 * Jakob Lövhall
 * Date: 2017-06-06
 */
public class Main {
    public static void main(String[] args) {
        ObjectMapper json = new ObjectMapper();
        Auth auth = null;
                
        try (InputStream is =  ClassLoader.getSystemClassLoader().getResourceAsStream("login.json")) {
            auth = json.readValue(is, Auth.class);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    
        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder.withToken(auth.getToken());
    
        Message messageListener = new Message();
        messageListener.addListener(new Math());
        messageListener.addListener(new Mastermind());
        clientBuilder.registerListener(messageListener);
        clientBuilder.registerListener(new Ready());
        
        clientBuilder.login();
        IDiscordClient client = clientBuilder.build();
    }
}
