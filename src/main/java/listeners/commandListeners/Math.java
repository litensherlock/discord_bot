package listeners.commandListeners;

import calculator.Calculator;
import calculator.InfixCalculator;
import listeners.Command;
import sx.blah.discord.handle.obj.IMessage;

import java.util.Optional;

/**
 * Jakob Lövhall
 * Date: 2017-07-01
 */
public class Math implements Command {
    private Calculator _calculator = new InfixCalculator();
    
    @Override
    public Optional<String> execute(String command, String param, IMessage ignored) {
        if ("calc".equals(command)) {
            return calculate(param);
        }
        return Optional.empty();
    }
    
    private Optional<String> calculate(String math){
        try {
            String answer = _calculator.calculate(math);
            return Optional.ofNullable(answer);
        } catch (Exception e){
            return Optional.of("could not calculate: " + math);
        }
    }
}
