package listeners.commandListeners;

import exceptions.WrongSize;
import listeners.Command;
import mastermind.Bot;
import mastermind.GameState;
import mastermind.ReturnPin;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Jakob Lövhall
 * Date: 2017-07-01
 */
public class Mastermind implements Command{
    final private Map<IUser, PlayerObject> map;
    final private Pattern p;
    
    public Mastermind() {
        map = new HashMap<>();
        p = Pattern.compile(".*?(\\d).*?(\\d).*?(\\d).*?(\\d)");
    }
    
    @Override
    public Optional<String> execute(String command, String parameter, IMessage message) {
        if (!"mastermind".equals(command)) {
            return Optional.empty();
        }
        
        IUser author = message.getAuthor();
        PlayerObject playerObject = map.computeIfAbsent(author, k -> new PlayerObject());
        return play(playerObject, parameter);
    }
    
    private Optional<String> play(PlayerObject playerObject, String parameter){
        Optional<String> s = handleParameters(playerObject, parameter);
        if (s.isPresent()){
            return s;
        }
        try {
            if (!playerObject.bot.playRound()) {
                List<Integer> lastGuess =
                        playerObject.gameState.getRows().get(
                        playerObject.gameState.currentRound());
                return Optional.of("I am guessing on " + lastGuess);
            } else {
                return Optional.of("It looks like I have won, woho!");
            }
        } catch (WrongSize wrongSize) {
            wrongSize.printStackTrace();
            return Optional.of("Something terrible happened so I have to exit this game right now. Good bye and have a good day.");
        }
    }
    
    private Optional<String> handleParameters(PlayerObject playerObject, String parameter) {
        if (parameter == null || parameter.isEmpty() ){
            return Optional.empty();
        }
        
        if ("reset".equalsIgnoreCase(parameter.trim())){
            playerObject.reset();
            return Optional.empty();
        }
        
        Matcher m = p.matcher(parameter);
        if (!m.matches()){
            return Optional.of("the response isn't formatted so that I understand it :/ \npleace format it as follows \"1, 2, 3, 1\" where 1 = correct color and place, 2 = correct color but wrong place, and 3 = the pin is the wrong color.");
        }
        
        List<ReturnPin> response = new ArrayList<>();
        response.add(stringToPin(m.group(1)));
        response.add(stringToPin(m.group(2)));
        response.add(stringToPin(m.group(3)));
        response.add(stringToPin(m.group(4)));
        
        playerObject.gameState.setNextResponse(response);
        return Optional.empty();
    }
    
    private ReturnPin stringToPin(String s){
        int i = Integer.parseInt(s);
        return intToReturnPin(i);
    }
    
    private ReturnPin intToReturnPin(int i){
        switch (i){
            case 1: return ReturnPin.CorrectColorAndPlace;
            case 2: return ReturnPin.CorrectColorWrongPlace;
            default: return ReturnPin.None;
        }
    }
    
    private class PlayerObject {
        Date created;
        GameState gameState;
        Bot bot;
    
        private PlayerObject() {
            gameState = new GameState();
            bot = new Bot(gameState);
            created = Date.from(Instant.now());
        }
        
        void reset(){
            gameState = new GameState();
            bot = new Bot(gameState);
        }
    }
}
