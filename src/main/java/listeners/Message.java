package listeners;

import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Jakob Lövhall
 * Date: 2017-06-10
 */
public class Message implements IListener<MessageReceivedEvent> {
    private final Pattern p;
    private final List<Command> listeners;
    
    /**
     * defaults to "!" as command prefix
     */
    public Message() {
        listeners = new ArrayList<>();
        p = getRegex("!");
    }
    
    public Message(String commandSeparator) {
        listeners = new ArrayList<>();
        p = getRegex(commandSeparator);
    }
    
    private Pattern getRegex(String commandPostfix) {
        return Pattern.compile("^" + commandPostfix + "(.+?)\\b(.*)");
    }
    
    public void handle(MessageReceivedEvent messageEvent) {
        try {
            String content = messageEvent.getMessage().getContent();
            Matcher m = p.matcher(content);
            if (m.matches()) {
                String command = m.group(1);
                String param = m.group(2);
                for (Command listener : listeners) {
                    Optional<String> answer = listener.execute(command, param, messageEvent.getMessage());
                    handleResponse(messageEvent, answer);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
    private void handleResponse(MessageReceivedEvent messageEvent, Optional<String> answer) {
        if (answer.isPresent()){
            new MessageBuilder(messageEvent.getClient()).
                    withChannel(messageEvent.getChannel()).
                    withContent(answer.get()).
                    build();
        }
    }
    
    public void addListener(Command command){
        listeners.add(command);
    }
}