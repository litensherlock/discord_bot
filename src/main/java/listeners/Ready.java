package listeners;

import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.ReadyEvent;

/**
 * Jakob Lövhall
 * Date: 2017-06-10
 */
public class Ready implements IListener<ReadyEvent> {
    @Override
    public void handle(ReadyEvent readyEvent) {
        System.out.println("ready event");
    }
}
