package listeners;

import sx.blah.discord.handle.obj.IMessage;

import java.util.Optional;

/**
 * Jakob Lövhall
 * Date: 2017-07-01
 */
public interface Command {
    Optional<String> execute(String command, String parameter, IMessage messageEvent);
}
